package com.mygdx.game.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.mygdx.game.Controller.ScreenTraveler;
import com.mygdx.game.Utils.Assets;

/**
 * Created by 06k1203 on 06.04.2017.
 */
public class Menu extends StdScreen {
    public Menu(SpriteBatch batch) {
        super(batch);
        Image image;
        Label label;
        Button button;

        Table layout = new Table();
        layout.setFillParent(true);
        layout.setDebug(true);


        ui.addActor(layout);

        button = new TextButton("Play", (TextButton.TextButtonStyle) Assets.get().btnStyles.get("menu"));
        button.addListener(new ScreenTraveler("Play"));
        layout.add(button).pad(10).row();

        button = new TextButton("Backgrounds", (TextButton.TextButtonStyle) Assets.get().btnStyles.get("menu"));
        button.addListener(new ScreenTraveler("Backgrounds"));
        layout.add(button).pad(10).row();

        stage.addActor(layout);

        button.addListener(new ScreenTraveler("Play"));
        button.setPosition(Gdx.graphics.getWidth() / 2 - button.getWidth() / 2, Gdx.graphics.getHeight() - 300);
        stage.addActor(button);
    }
}