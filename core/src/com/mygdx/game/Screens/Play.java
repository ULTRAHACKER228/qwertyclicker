package com.mygdx.game.Screens;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.mygdx.game.Model.World;
import com.mygdx.game.Utils.Assets;
import com.mygdx.game.Utils.Values;

/**
 * Created by 06k1203 on 06.04.2017.
 */
public class Play extends StdScreen {
    private World world;
    private Label score;
    public Play(SpriteBatch batch) {
        super(batch);

        world = new World(stage.getViewport(), stage.getBatch());
        stage = world;

        Image image;
        Button button;

        Table layout = new Table();
        layout.setFillParent(true);
        layout.setDebug(true);

        score = new Label("0", Assets.get().labelStyles.get("score"));
        layout.add(score).row();
        layout.add().expand().row();


        ui.addActor(layout);

        Image bgImage = new Image(Assets.get().imgs.get("backgrounds/0"));
        bgImage.setSize(Values.WORLD_WIDTH, Values.WORLD_HEIGHT);

        bg.addActor(bgImage);

    }

    public void render(float delta) {
        super.act(delta);
        if (world.scoreUpdated()) score.setText(String.format("%d", world.getScore()));
        super.draw();
    }

}
