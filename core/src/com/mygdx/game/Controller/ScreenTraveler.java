package com.mygdx.game.Controller;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.mygdx.game.QwertyClicker;
import com.mygdx.game.Utils.Assets;

/**
 * Created by 06k1203 on 06.04.2017.
 */
public class ScreenTraveler extends ClickListener {
    private String name;
    public ScreenTraveler(String name){
        super();
        this.name = name;
    }
    public void clicked(InputEvent event, float x, float y){
        Assets.get().sounds.get("Click1").play();
        QwertyClicker.get().setScreen(name);
    }
}
