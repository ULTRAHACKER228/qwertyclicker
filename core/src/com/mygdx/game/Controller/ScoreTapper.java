package com.mygdx.game.Controller;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.mygdx.game.Model.World;
import com.mygdx.game.Utils.Assets;

import java.util.Random;


/**
 * Created by 06k1203 on 20.04.2017.
 */
public class ScoreTapper extends ClickListener {
    private World world;
    private int amount;
    private TextButton clicker;
    private Random r;

    public ScoreTapper(World world, int amount, TextButton clicker) {
        this.world = world;
        this.amount = amount;
        this.clicker = clicker;
        r = new Random();
    }

    public void clicked(InputEvent event, float x, float y) {
        Assets.get().sounds.get("Click2").play();
        for (int i = 0; i < amount; i++)
            world.tap();
        clicker.setText(String.format("%c", 'A' + r.nextInt(26)));
    }
}
