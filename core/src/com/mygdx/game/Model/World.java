package com.mygdx.game.Model;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.mygdx.game.Controller.ScoreTapper;
import com.mygdx.game.Utils.Assets;

/**
 * Created by 06k1203 on 06.04.2017.
 */
public class World extends Stage {
    private int score;
    private boolean scoreUpd;
    private TextButton clicker;

    public World(Viewport viewport, Batch batch) {
        super(viewport, batch);
        Table layout = new Table();
        layout.setFillParent(true);
        clicker = new TextButton("A", (TextButton.TextButtonStyle) Assets.get().btnStyles.get("play"));
        clicker.addListener(new ScoreTapper(this, 100, clicker));
        layout.add(clicker).size(300);
        addActor(layout);
    }

    public int getScore() {
        scoreUpd = false;
        return score;
    }

    public boolean scoreUpdated() {
        return scoreUpd;
    }

    public void tap() {
        scoreUpd = true;
        score++;
    }
}
