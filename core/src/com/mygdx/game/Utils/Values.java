package com.mygdx.game.Utils;

import com.badlogic.gdx.Gdx;

/**
 * Created by 06k1203 on 06.04.2017.
 */
public class Values {
    public static final float WORLD_WIDTH = 540;
    public static final float WORLD_HEIGHT = 960;
    public static final float ppuX = Gdx.graphics.getWidth()/WORLD_WIDTH;
    public static final float ppuY = Gdx.graphics.getHeight()/WORLD_HEIGHT;
}