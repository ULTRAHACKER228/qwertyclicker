package com.mygdx.game.Utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

import java.util.HashMap;

/**
 * Created by 06k1203 on 06.04.2017.
 */
public class Assets {
    private static Assets ourInstance = new Assets();

    public static Assets get() {
        return ourInstance;
    }

    private Assets() {
        initImages();
        initFonts();
        initBtnStyles();
        initLabelStyles();
        initSounds();

    }

    private void initSounds() {
        sounds = new HashMap<String, Sound>();
        sounds.put("Click1", Gdx.audio.newSound(getHandle("Sounds/Click/Click1.mp3")));
        sounds.put("Click2", Gdx.audio.newSound(getHandle("Sounds/Click/Click2.mp3")));

        music = new HashMap<String, Music>();
        music.put("Bg1", Gdx.audio.newMusic(getHandle("Sounds/BGmusic/BG1.mp3")));
        music.put("Bg2", Gdx.audio.newMusic(getHandle("Sounds/BGmusic/BG2.mp3")));
        music.put("Bg3", Gdx.audio.newMusic(getHandle("Sounds/BGmusic/BG3.mp3")));
        music.put("Bg4", Gdx.audio.newMusic(getHandle("Sounds/BGmusic/BG4.mp3")));
    }

    private void initLabelStyles() {
        labelStyles = new HashMap<String, Label.LabelStyle>();
        labelStyles.put("score", new Label.LabelStyle(fonts.get("normal"), Color.GREEN));
    }

    private void initBtnStyles() {
        btnStyles = new HashMap<String, Button.ButtonStyle>();
        btnStyles.put("play", new TextButton.TextButtonStyle(
                imgs.get("ui/btn-up"),
                imgs.get("ui/btn-down"),
                null,
                fonts.get("normal")
        ));
        btnStyles.put("menu", new TextButton.TextButtonStyle(
                imgs.get("ui/menu-up"),
                imgs.get("ui/menu-down"),
                null,
                fonts.get("normal")
        ));
    }

    private void initFonts() {
        fonts = new HashMap<String, BitmapFont>();
        fonts.put("normal", new BitmapFont());
    }

    private void initImages() {
        imgs = new HashMap<String, TextureRegionDrawable>();
        addFolderImg(getHandle("ui/"), "");
        addFolderImg(getHandle("buttons/"), "");
        addFolderImg(getHandle("backgrounds/"), "");
        for (String name : imgs.keySet())
            System.out.println(name);

    }

    private void addFolderImg(FileHandle file, String prefix) {
        if (file.isDirectory())
            for (FileHandle f : file.list())
                addFolderImg(f, prefix + file.name());
        else if (file.extension().equals("png") || file.extension().equals("jpg"))
            imgs.put(prefix + "/" + file.nameWithoutExtension(), makeDrawable(file));
    }

    private TextureRegionDrawable makeDrawable(FileHandle handle) {
        return new TextureRegionDrawable(new TextureRegion(new Texture(handle)));
    }

    private FileHandle getHandle(String fileName) {
        return Gdx.files.internal(fileName);
    }

    public HashMap<String, TextureRegionDrawable> imgs;
    public HashMap<String, Button.ButtonStyle> btnStyles;
    public HashMap<String, Label.LabelStyle> labelStyles;
    public HashMap<String, BitmapFont> fonts;
    public HashMap<String, Music> music;
    public HashMap<String, Sound> sounds;
}