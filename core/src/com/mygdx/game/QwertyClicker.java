package com.mygdx.game;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.mygdx.game.Screens.Menu;
import com.mygdx.game.Screens.Play;

import java.util.HashMap;


public class QwertyClicker extends Game {
	public static QwertyClicker instance = new QwertyClicker();
	private QwertyClicker(){}
	public static QwertyClicker get(){return instance;}

	@Override
	public void create () {
		batch = new SpriteBatch();
		screens = new HashMap<String, Screen>();
		screens.put("Menu", new Menu(batch));
		screens.put("Play", new Play(batch));

		setScreen("Play");
	}

	public void setScreen(String name){
		if (screens.containsKey(name))
			setScreen(screens.get(name));
	}
	private SpriteBatch batch;
	private HashMap<String, Screen> screens;

}